import React from 'react'
import "./VerticalStepper.css"

export default function VerticalStepper() {
  return (
    // <div className='VerticalStepperTop'>
        <div class="stepper d-flex flex-column mt-5 ml-2">
    <div class="d-flex mb-1">
      <div class="d-flex flex-column pr-4 align-items-center">
        <div class="rounded-circle py-2 px-3 bg-primary text-white mb-1">1</div>
        <div class="line h-100"></div>
      </div>
      <div>
        <h5 class="text-dark text-start px-4 StepperMainLabels">Foundation (2 Courses)</h5>
        <p class="lead text-muted pb-3 px-4 subtextMainLabels">These foundation courses cover introductory part about HPC domain</p>

        <div className="coursesVerticalStepper">
        <div className="">
                  <div className="row">
                  <div class="col-4 mx-auto mt-3">
            <div class="card card2">
                <img src={require("../../Images/Mask2.png")} class="card-img-top" alt="..."></img>
                    <div class="card-body">
                        <h5 class="card-title card2-title">CUDA programming</h5>
                    </div>
            </div>
        </div>
        <div class="col-4 mx-auto mt-3">
            <div class="card card2">
                <img src={require("../../Images/Mask2.png")} class="card-img-top" alt="..."></img>
                    <div class="card-body">
                        <h5 class="card-title card2-title">SPACK utility</h5>
                    </div>
            </div>
        </div>
                  </div>
              </div>
        </div>
        
      </div>

      {/* <div className="coursesVerticalStepper">
          
      </div> */}
    </div>
    <div class="d-flex mb-1">
      <div class="d-flex flex-column pr-4 align-items-center">
        <div class="rounded-circle py-2 px-3 bg-primary text-white mb-1">2</div>
        <div class="line h-100"></div>
      </div>
      <div>
        <h5 class="text-dark text-start px-4 StepperMainLabels">Intermediatory (2 Courses)</h5>
        <p class="lead text-muted pb-3 px-4 subtextMainLabels">These intermediatory courses covers about OpenMP and MPI</p>

        <div className="coursesVerticalStepper">
        <div className="">
                  <div className="row">
                  <div class="col-4 mx-auto mt-3">
            <div class="card card2">
                <img src={require("../../Images/Mask2.png")} class="card-img-top" alt="..."></img>
                    <div class="card-body">
                        <h5 class="card-title card2-title">CUDA programming</h5>
                    </div>
            </div>
        </div>
        <div class="col-4 mx-auto mt-3">
            <div class="card card2">
                <img src={require("../../Images/Mask2.png")} class="card-img-top" alt="..."></img>
                    <div class="card-body">
                        <h5 class="card-title card2-title">SPACK utility</h5>
                    </div>
            </div>
        </div>
                  </div>
              </div>
        </div>
        
      </div>

      {/* <div className="coursesVerticalStepper">
          
      </div> */}
    </div>
    <div class="d-flex mb-1">
      <div class="d-flex flex-column pr-4 align-items-center">
        <div class="rounded-circle p-2 px-3 bg-primary text-white mb-1">3</div>
        {/* <div class="line h-100"></div> */}
      </div>
      <div>
        <h5 class="text-dark text-start px-4 StepperMainLabels">Advanced (2 Courses)</h5>
        <p class="lead text-muted pb-3 subtextMainLabels">These courses cover about advanced part of HPC domain</p>

        <div className="coursesVerticalStepper">
        <div className="">
                  <div className="row">
                  <div class="col-4 mx-auto mt-3">
            <div class="card card2">
                <img src={require("../../Images/Mask2.png")} class="card-img-top" alt="..."></img>
                    <div class="card-body">
                        <h5 class="card-title card2-title">CUDA programming</h5>
                    </div>
            </div>
        </div>
        <div class="col-4 mx-auto mt-3">
            <div class="card card2">
                <img src={require("../../Images/Mask2.png")} class="card-img-top" alt="..."></img>
                    <div class="card-body">
                        <h5 class="card-title card2-title">SPACK utility</h5>
                    </div>
            </div>
        </div>
                  </div>
              </div>
        </div>
        
      </div>

      {/* <div className="coursesVerticalStepper">
          
      </div> */}
    </div>
  </div>
    // </div>
  )
}
