import './App.css';
import Navbar from './Components/Navbar/Navbar';
import Topbar from './Components/Topbar/Topbar';
import Home from './Pages/Home/Home';
import LearningPathHpc from './Pages/LearningPathHPC/LearningPathHpc';
import Message from './Pages/Message/Message';
import Registration from './Pages/Registration/Registration';

function App() {
  return (
    <div className="App">
      {/* <Home></Home> */}
      {/* <Registration></Registration> */}
      {/* <Message></Message> */}
      <LearningPathHpc></LearningPathHpc>
    </div>
  );
}

export default App;
