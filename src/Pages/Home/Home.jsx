import React from 'react';
import Cards from '../../Components/Cards/Cards';
import Cards2 from '../../Components/Cards2/Cards2';
import Footer from '../../Components/Footer/Footer';
import ImageSlider from '../../Components/ImageSlider/ImageSlider';
import InstructorSlider from '../../Components/InstructorSlider/InstructorSlider';
import LPathHome from '../../Components/LPathHome/LPathHome';
import Navbar from '../../Components/Navbar/Navbar';

export default function Home() {
  return <div>
      <Navbar></Navbar>
      <ImageSlider></ImageSlider>
      <Cards></Cards>
      <Cards2></Cards2>
      <LPathHome></LPathHome>
      <InstructorSlider></InstructorSlider>
      <Footer></Footer>
  </div>;
}
