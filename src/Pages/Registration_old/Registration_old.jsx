import React from 'react';
import Container from 'react-bootstrap/Container';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import { useState } from 'react';
import ReCAPTCHA from "react-google-recaptcha";

export default function Registration() {
    const [verified, setVerified] = useState(false);

    function onChange(value) {
      console.log("Captcha value:", value);
    }
  
    return (
      <Container id="main-container" className="d-grid h-100">
        <Form>
          
          <Form.Group className="mb-3" controlId="Form.ControlInput1">
            <Form.Label>Name</Form.Label>
            <Form.Control type="text" placeholder="name" />
          </Form.Group>
          
          <Form.Group className="mb-3" controlId="Form.ControlInput2">
            <Form.Label>Email address</Form.Label>
            <Form.Control type="email" placeholder="email address" />
          </Form.Group>
          
          <Form.Group className="mb-3" controlId="Form.ControlInput3">
            <Form.Label>Passowrd</Form.Label>
            <Form.Control type="password" placeholder="password" />
          </Form.Group>
  
          <Form.Group className="mb-3" controlId="Form.ControlInput4">
            <Form.Label>Institute</Form.Label>
            <Form.Control type="text" placeholder="Institute name" />
          </Form.Group>
  
          <Form.Group className="mb-3" controlId="exampleForm.ControlTextarea1">
            <Form.Label>Address</Form.Label>
            <Form.Control as="textarea" rows={3} />
          </Form.Group>
  
          <Form.Group className="mb-3" controlId="Form.ControlInput5">
            <Form.Label>HOD Email address</Form.Label>
            <Form.Control type="email" placeholder="HOD email address" />
          </Form.Group>
  
          <Form.Group controlId="formFile" className="mb-3">
            <Form.Label>Aadhar Card</Form.Label>
            <Form.Control type="file" />
          </Form.Group>
          
          <Form.Group controlId="formFile" className="mb-3">
            <Form.Label>Photo</Form.Label>
            <Form.Control type="file" />
          </Form.Group>
  
          <Form.Label htmlFor="inputEdu">Educational Qualification</Form.Label>
          <Form.Select aria-label="Default select example">
            <option value="1">Educational Qualification 1</option>
            <option value="2">Educational Qualification 2</option>
            <option value="3">Educational Qualification 3</option>
            <option value="4">Educational Qualification 4</option>
          </Form.Select>
  
          <br></br>
          
          <Form.Label htmlFor="inputDomain">Domain</Form.Label>
          <Form.Select aria-label="Default select example">
            <option value="1">Domain 1</option>
            <option value="2">Domain 2</option>
            <option value="3">Domain 3</option>
            <option value="4">Domain 4</option>
          </Form.Select>
  
          <br></br>
  
          <ReCAPTCHA
            sitekey="6LeIxAcTAAAAAJcZVRqyHh71UMIEGNQ_MXjiZKhI"
            onChange={onChange}
          />
  
          <div className="d-grid mb-3">
            <Button variant="primary" size="lg" disabled={!verified}>Register</Button>
          </div>
  
          <br></br>
        </Form>
      </Container>
    );
}
