import React from 'react';
import { useState } from 'react';
import ReCAPTCHA from "react-google-recaptcha";
import "./Registration.css"

export default function Registration() {

  
    return (
      <div className="registrationTopDiv">
        <div className="container">
          <div className="row rowForm">
            <div className="col-5">
                    <div class="container">
                    	{/* <a href="#" class="avartar">
                    		<img src="images/avartar.png" alt="">
                    	</a> */}
                    	<div class="row form-row form-group">
                    		<div class="form-holder col">
                    			<input type="text" class="form-control" placeholder="First Name"></input>
                    		</div>
                    		<div class="form-holder col">
                    			<input type="text" class="form-control" placeholder="Last Name"></input>
                    		</div>
                    	</div>
                    	<div class="form-row">
                    		<div class="form-holder">
                    			<input type="password" class="form-control" placeholder="Password"></input>
                    			<i class="fa-solid fa-lock"></i>
                    		</div>
                    	</div>
                    	<div class="form-row">
                    		<div class="form-holder">
                    			<input type="text" class="form-control" placeholder="Email"></input>
                    			<i class="fa-solid fa-envelope"></i>
                    		</div>
                    	</div>
                    	<div class="form-row">
                    		<div class="form-holder">
                    			<input type="text" class="form-control" placeholder="Phone"></input>
                    			<i class="fa-solid fa-mobile-screen-button"></i>
                    		</div>
                    	</div>
                    	<div class="form-row">
                    		<div class="form-holder">
                    			<input type="text" class="form-control" placeholder="Address"></input>
                          <i class="fa-solid fa-map"></i>
                    		</div>
                    	</div>
                    	<div class="form-row">
                    		<div class="form-holder">
                    			<input type="text" class="form-control" placeholder="Nation"></input>
                          <i class="fa-solid fa-circle-user"></i>

                    		</div>
                    	</div>
                    </div>
            </div>
            {/* <div className="col-2 middleColumn"></div> */}
            <div className="col-5">
            <div class="container">
                    	{/* <a href="#" class="avartar">
                    		<img src="images/avartar.png" alt="">
                    	</a> */}
                    	<div class="row form-row form-group">
                    		<div class="form-holder col">
                    			<input type="text" class="form-control" placeholder="First Name"></input>
                    		</div>
                    		<div class="form-holder col">
                    			<input type="text" class="form-control" placeholder="Last Name"></input>
                    		</div>
                    	</div>
                    	<div class="form-row">
                    		<div class="form-holder">
                    			<input type="password" class="form-control" placeholder="Password"></input>
                    			<i class="fa-solid fa-lock"></i>
                    		</div>
                    	</div>
                    	<div class="form-row">
                    		<div class="form-holder">
                    			<input type="text" class="form-control" placeholder="Email"></input>
                    			<i class="fa-solid fa-envelope"></i>
                    		</div>
                    	</div>
                    	<div class="form-row">
                    		<div class="form-holder">
                    			<input type="text" class="form-control" placeholder="Phone"></input>
                    			<i class="zmdi zmdi-smartphone-android"></i>
                    		</div>
                    	</div>
                    	<div class="form-row">
                    		<div class="form-holder">
                    			<input type="text" class="form-control" placeholder="Address"></input>
                                <i class="zmdi zmdi-map small"></i>
                    		</div>
                    	</div>
                    	<div class="form-row">
                    		<div class="form-holder">
                    			<input type="text" class="form-control" placeholder="Nation"></input>
                                <i class="zmdi zmdi-account-box-o small"></i>

                    		</div>
                    	</div>
                    </div>
            </div>
          </div>
        </div>
      </div>
    );
}
