import React from 'react';
import { useState } from 'react';
import ReCAPTCHA from "react-google-recaptcha";
import { useRef } from 'react';
import "./Registration.css";
// import "./script.js";

export default function Registration() {


    const myElement = useRef(null);
    

    function nextButton() {
        // const form = myElement.current.querySelector("form");
        // const nextBtn = myElement.current.querySelector(".nextBtn");
        // const backBtn = myElement.current.querySelector(".backBtn");
        // const allInput = myElement.current.querySelectorAll(".first input");
        // allInput.forEach(input => {
        //     if(input.value != ""){
        //         form.classList.add('secActive');
        //     }else{
        //         form.classList.remove('secActive');
        //     }
        // })
        console.log("in next button")
      }

    function backButton() {
        const form = myElement.current.querySelector("form");
        form.classList.remove('secActive');
      }
  
    return (
		<div className='RegistrationBody'>
		<div class="container RegistrationContainer">
        <header>Registration</header>

        <form action="#">
            <div class="form first">
                <div class="details personal">
                    <span class="title">Personal Details</span>

                    <div class="fields">
                        <div class="input-field">
                            <label>Full Name</label>
                            <input type="text" placeholder="Enter your name"></input>
                        </div>

                        <div class="input-field">
                            <label>Date of Birth</label>
                            <input type="date" placeholder="Enter birth date"></input>
                        </div>

                        <div class="input-field">
                            <label>Email</label>
                            <input type="text" placeholder="Enter your email"></input>
                        </div>

                        <div class="input-field">
                            <label>Mobile Number</label>
                            <input type="number" placeholder="Enter mobile number"></input>
                        </div>

                        <div class="input-field">
                            <label>Gender</label>
                            <select>
                                <option disabled selected>Select gender</option>
                                <option>Male</option>
                                <option>Female</option>
                                <option>Others</option>
                            </select>
                        </div>

                        <div class="input-field">
                            <label>Occupation</label>
                            <input type="text" placeholder="Enter your ccupation"></input>
                        </div>
                    </div>
                </div>

                <div class="details ID">
                    <span class="title">Identity Details</span>

                    <div class="fields">
                        <div class="input-field">
                            <label>ID Type</label>
                            <input type="text" placeholder="Enter ID type"></input>
                        </div>

                        <div class="input-field">
                            <label>ID Number</label>
                            <input type="number" placeholder="Enter ID number"></input>
                        </div>

                        <div class="input-field">
                            <label>Issued Authority</label>
                            <input type="text" placeholder="Enter issued authority"></input>
                        </div>

                        <div class="input-field">
                            <label>Issued State</label>
                            <input type="text" placeholder="Enter issued state"></input>
                        </div>

                        <div class="input-field">
                            <label>Issued Date</label>
                            <input type="date" placeholder="Enter your issued date"></input>
                        </div>

                        <div class="input-field">
                            <label>Expiry Date</label>
                            <input type="date" placeholder="Enter expiry date"></input>
                        </div>
                    </div>

                    <button class="nextBtn">
                        <span class="btnText" onClick={nextButton}>Next</span>
                        {/* <i class="uil uil-navigator"></i> */}
						{/* <i class="fa-solid fa-chevrons-right"></i> */}
						<i class="fa-regular fa-circle-right"></i>
						{/* <FontAwesomeIcon icon="fa-solid fa-chevrons-right" /> */}
						{/* <i class="fa-regular fa-arrow-right-from-bracket"></i> */}
                    </button>
                </div> 
            </div>

            <div class="form second">
                <div class="details address">
                    <span class="title">Address Details</span>

                    <div class="fields">
                        <div class="input-field">
                            <label>Address Type</label>
                            <input type="text" placeholder="Permanent or Temporary"></input>
                        </div>

                        <div class="input-field">
                            <label>Nationality</label>
                            <input type="text" placeholder="Enter nationality"></input>
                        </div>

                        <div class="input-field">
                            <label>State</label>
                            <input type="text" placeholder="Enter your state"></input>
                        </div>

                        <div class="input-field">
                            <label>District</label>
                            <input type="text" placeholder="Enter your district"></input>
                        </div>

                        <div class="input-field">
                            <label>Block Number</label>
                            <input type="number" placeholder="Enter block number"></input>
                        </div>

                        <div class="input-field">
                            <label>Ward Number</label>
                            <input type="number" placeholder="Enter ward number"></input>
                        </div>
                    </div>
                </div>

                <div class="details family">
                    <span class="title">Family Details</span>

                    <div class="fields">
                        <div class="input-field">
                            <label>Father Name</label>
                            <input type="text" placeholder="Enter father name"></input>
                        </div>

                        <div class="input-field">
                            <label>Mother Name</label>
                            <input type="text" placeholder="Enter mother name"></input>
                        </div>

                        <div class="input-field">
                            <label>Grandfather</label>
                            <input type="text" placeholder="Enter grandfther name"></input>
                        </div>

                        <div class="input-field">
                            <label>Spouse Name</label>
                            <input type="text" placeholder="Enter spouse name"></input>
                        </div>

                        <div class="input-field">
                            <label>Father in Law</label>
                            <input type="text" placeholder="Father in law name"></input>
                        </div>

                        <div class="input-field">
                            <label>Mother in Law</label>
                            <input type="text" placeholder="Mother in law name"></input>
                        </div>
                    </div>

                    <div class="buttons">
                        <div class="backBtn">
                            {/* <i class="uil uil-navigator"></i> */}
                            <i class="fa-regular fa-circle-left"></i>
                            <span class="btnText" onClick={backButton}>Back</span>
                        </div>
                        
                        <button class="sumbit">
                            <span class="btnText">Submit</span>
                            {/* <i class="uil uil-navigator"></i> */}
                            <i class="fa-regular fa-circle-right"></i>
                        </button>
                    </div>
                </div> 
            </div>
        </form>
    </div>
	</div>
    );
}
